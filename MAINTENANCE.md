# Maintaining this handbook over time

There are a few things that are guaranteed to change every release.
This is a short list of them, to make such maintenance easier.

## Release Version
The release version is written down in a few places.
These are:
* modules/Working/pages/apk.doc (including the previous version)
* modules/Installing/examples/repositories.apk

It's ok if the version examples lag a bit behind, but we should never mention versions that are no longer supported.
